<?php
/**
* HEX to RGB
* @return String
*/
function hext_to_rgb($hex){
	$sscanf_format = (4 > strlen($hex)) ? "#%02x%02x%02x" : "#%1x%1x%1x";
	list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
	return "$r, $g, $b";
}