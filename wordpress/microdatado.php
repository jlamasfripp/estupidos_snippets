    <?php
      // buscamos la categoría principal de la página
      $mainEntityOfPage = str_replace("/./", "/", get_term_link(get_the_category()[0]));
    ?>
    <script type="application/ld+json">
      {
         "@context": "https://schema.org",
         "@type": "NewsArticle",
          "mainEntityOfPage": {
            "@type": "WebPage",
            "@id": "<?php echo $mainEntityOfPage; ?>"
          },
         "headline": "<?php echo get_the_title(); ?>",
         "image": [
           "<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>",
           "<?php echo get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>",
           "<?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail'); ?>"
         ],
         "datePublished": "<?php echo get_the_date('Y-m-d\TH:i:s\Z'); ?>",
         "dateModified": "<?php echo get_the_modified_date('Y-m-d\TH:i:s\Z'); ?>",
         "author": {
           "@type": "Person",
           "name": "<?php echo get_the_author(); ?>"
         },
         "publisher": {
           "@type": "Organization",
           "name": "jlamasfripp",
           "logo": {
             "@type": "ImageObject",
             "url": "<?php echo get_stylesheet_directory_uri(); ?>/logo.png"
           }
         }
       }