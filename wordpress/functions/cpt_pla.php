<?php
// Declarar en functions con prefijo personal (wololo)
  if ( ! function_exists('wololo')) {
    function wololo(){

    }
    add_action('', 'wololo'); // 'init', por ejemplo
  }
// Registrar sidebar
  register_sidebar([
    'name'          => __('', ''),
    'id'            => '',
    'description'   => __('', ''),
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '',
    'after_title'   => '',
  ]);
// Declaramos post_type (wawa)
  if ( ! function_exists('wololo_wawa_post_type')) {
    function wololo_wawa_post_type(){
        $labels  = array(
            'name'  => __('Wawas','wordtest'),
            'singular_name'  => __('Wawa','wordtest'),
            'add_new'  => __('Añadir Wawa','wordtest'),
            'all_items' =>__('Wawas', 'wordtest'),
            'add_new_item' =>__('Añadir Wawa', 'wordtest'),
            'edit_item' =>__('Editar Wawa', 'wordtest'),
            'new_item' =>__('Nuevo Wawa', 'wordtest'),
            'view_item' =>__('Ver Wawa', 'wordtest'),
            'search_item' =>__('Buscar Wawa', 'wordtest'),
            'not_found' =>__('No encontrado', 'wordtest'),
            'not_found_in_trash' => __('No hay wawa en la papelera', 'wordtest'),
            'parent_item_colon' =>__('Wawa padre', 'wordtest')
          );
        $args = array(
          'labels' => $labels,
          'public'  => true,
          'has_archive'  => true,
          'publicly_queryable'  => true,
          'query_var' => true,
          'rewrite' => true,
          'capability_type' => 'post',
          'hierarchical' => false,
          'supports'  => array(
              'title',
              'editor',
              'excerpt',
              'thumbnail',
              'revisions',
              'custom-fields',
              'author',
              'page-attributes'
          ),
          // 'taxonomies'  =>  array('category', 'post_tag'),
          'menu_position'  => 5,
          'exclude_from_search'  => false,
          'show_ui'  => true,
          'show_in_nav_menus'  => true,
          'show_in_menu'  => true,
          'show_in admin_bar'  => true,
          'show_in_menu'  => true,
          'show_in_nav_menus'  => true,
          'menu_icon'  => 'dashicons-book-alt',
          'show_in_rest' => true
        );
        register_post_type('wawa', $args );
    }
    add_action('init', 'wololo_wawa_post_type');
  }

  // Taxonomías
  if ( ! function_exists('wololo_custom_taxonomy')) {
    function wololo_custom_taxonomy(){
      // nueva taxonomía jerárquica (categoría = tipo)
      $labels = array(
          'name'  => __('Tipos','wordtest'),
          'singular_name'  => __('Tipo','wordtest'),
          'add_new'  => __('Añadir Tipo','wordtest'),
          'all_items' =>__('Tipos', 'wordtest'),
          'add_new_item' =>__('Añadir Tipo', 'wordtest'),
          'edit_item' =>__('Editar Tipo', 'wordtest'),
          'new_item' =>__('Nuevo Tipo', 'wordtest'),
          'view_item' =>__('Ver Tipo', 'wordtest'),
          'search_item' =>__('Buscar Tipo', 'wordtest'),
          'not_found' =>__('No encontrado', 'wordtest'),
          'not_found_in_trash' => __('No hay tipos en la papelera', 'wordtest'),
          'parent_item' =>__('Tipo padre', 'wordtest'),
          'parent_item_colon' =>__('Tipo padre:', 'wordtest'),
          'menu_name' => __('Tipo', 'wordtest')
        );
      $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui'  => true,
        'show_admin_column'  => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'tipos'),
        'show_in_rest' => true,
        'capabilities' => [
          'manage_terms' => 'administrator',
          'edit_terms' => 'administrator',
          'delete_terms' => 'administrator',
          'assign_terms' => 'administrator', 'editor', 'author', 'contibutor'
          ],
      );

      register_taxonomy('tipos', ['wawa'], $args);

      // nueva taxonomía NO jerárquica (tag = fufu)

      register_taxonomy('fufu', ['wawa'], [
        'label' => 'Fufus',
        'rewrite' => ['slug' => 'fufu'],
        'hierarchical' => false,
      ]);


    }
    add_action('init', 'wololo_custom_taxonomy');
  }
