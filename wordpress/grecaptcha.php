<style>
/* grecaptchaBox - Esto va en styles.css o en ehemes/styles/estilos.css */
.grecaptcha-box {
  position: relative;
}

.grecaptcha-box > .grecaptcha-badge {
  position: absolute !important;
}
.grecaptcha-badge {
  bottom: 228px !important;
}
</style>
<script>
/* grecaptchaBox - Eso va en theme/js/grecaptcha-box.js */
function grecaptchaBox() {
  let grecaptcha_badge = document.querySelector('.grecaptcha-badge');
  if (null == grecaptcha_badge) return false;
  grecaptcha_badge.parentElement.classList.add('grecaptcha-box');
}

// reposicionar recaptcha
grecaptcha.ready(grecaptchaBox);
</script>
<?php
/**
* Encolar script para reposicionar google recaptcha
*/
if ( !function_exists( 'set_grecaptcha_box' ) ) {
  function set_grecaptcha_box()
  {
    // Register the script
    wp_register_script(
      'grecaptcha_box',
      trailingslashit( get_stylesheet_directory_uri() ) . 'js/grecaptcha-box.js',
      array(  ),
      '0.0.1',
      false
    );
  }
}

add_action( 'wp_enqueue_scripts', 'set_grecaptcha_box', 1001 );