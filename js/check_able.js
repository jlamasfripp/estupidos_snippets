// Función para jQuery que permite o impide 
// editar un input-group según esté checkeado o no
// y añade boolean para confirmar en algún framework.

function ckeck_able(){
    $(".ckeck_able").attr("disabled", true);
    $("input:checkbox").on("click", function () {
    if($(this).is(':checked')){
        $(this).parents().eq(1).next().attr("disabled", false);
        $(this).val(true);
    }else{
        $(this).parents().eq(1).next().attr("disabled", true);
        $(this).val(false);
    }
    });
}